const mongoose = require('mongoose');
const config = require('./config');
const User = require("./models/User");
const Message = require("./models/Message");
const {nanoid} = require('nanoid');

const run = async () => {
  await mongoose.connect(config.db.url, config.db.options);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [user, admin] = await User.create({
    email: 'user@test',
    password: '123',
    token: nanoid(),
    role: 'user',
    displayName: 'user'
  }, {
    email: 'admin@test',
    password: '123',
    token: nanoid(),
    role: 'admin',
    displayName: 'admin'
  });

  await Message.create({
    message: "Используйте разные браузеры или режим инкогнито для тестов пользователей чата",
    user: "Developer",
    userId: admin
  });

  await mongoose.connection.close();
};

run().catch(e => console.error(e));
