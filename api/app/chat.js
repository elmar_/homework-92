const express = require('express');

const router = express.Router();

require('express-ws')(router);

router.ws('/', (ws, req) => {
  console.log(req);

  ws.send('message from api');

  ws.on('message', msg => {
    console.log('message received', msg);
  });
});



module.exports = router;
