const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const exitHook = require('async-exit-hook');
const users = require('./app/users');
const config = require('./config');
const User = require("./models/User");
const Message = require("./models/Message");

const app = express();
app.use(express.json());
app.use(cors());

const port = 8000;

app.use('/users', users);

require('express-ws')(app);

const activeConnections = {};

app.ws('/chat', async (ws, req) => {

  try {
    const token = req.query.token;
    const user = await User.findOne({token});

    if (!user) {
      ws.send(JSON.stringify({type: "SEND_MESSAGE_ERROR", message: 'Not found user'}));
      return ws.close();
    }

    const id = user._id;
    activeConnections[id] = {ws, user: user.displayName};
    const firstMessages = await Message.find({}).limit(30);

    const  usersToSend = [];

    Object.keys(activeConnections).forEach(key => {
      usersToSend.push(activeConnections[key].user);
    });

    Object.keys(activeConnections).forEach(key => {
      if (key !== id) {
        const connection = activeConnections[key].ws;
        connection.send(JSON.stringify({
          type: 'GET_USER',
          user: user.displayName
        }));
      }
    });

    ws.send(JSON.stringify({type: "GET_ALL_MESSAGES", messages: firstMessages}));
    ws.send(JSON.stringify({type: "GET_ALL_USERS", users: usersToSend}));

    ws.on('message', async msg => {
      try {
        const comeMessage = JSON.parse(msg);

        await Message.create({
          message: comeMessage.message,
          user: comeMessage.user.displayName,
          userId: id
        });

        Object.keys(activeConnections).forEach(key => {
          const connection = activeConnections[key].ws;
          connection.send(JSON.stringify({
            type: 'GET_MESSAGE_SUCCESS',
            message: {
              message: comeMessage.message,
              user: user.displayName,
            }
          }));
        });

      } catch (e) {
        console.error(e);
      }
    });

    ws.on('close', ws => {
      const index = usersToSend.findIndex(el => el === user.displayName);
      usersToSend.splice(index, 1);
      //ws.send(JSON.stringify({type: "GET_ALL_USERS", users: usersToSend}));
      delete activeConnections[id];
    });

  } catch (e) {
    ws.send(JSON.stringify({type: "SEND_MESSAGE_ERROR", global: 'Not found user!!'}));
    return ws.close();
  }
});

const run = async () => {
  await mongoose.connect(config.db.url, config.db.options);

  app.listen(port, () => {
    console.log("We are on port " + port);
  });

  exitHook(async callback => {
    await mongoose.disconnect();
    console.log('mongoose is disconnected');
    callback();
  });
};

run().catch(e => console.error(e));

