module.exports = {
  db: {
    url: 'mongodb://localhost/homework-92',
    options: {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    }
  },
};
