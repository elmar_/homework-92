import React from 'react';
import {Button, Col, Layout, Row, Space} from "antd";
import {logoutUser} from "../../store/actions/usersActions";
import {useDispatch, useSelector} from "react-redux";

const { Header, Content, Footer } = Layout;

const Lay = ({children}) => {

  const user = useSelector(state => state.users.user);
  const dispatch = useDispatch();

  return (
    <div>
      <Layout>
        <Header style={{backgroundColor: '#fff'}}>
          <Row>
            <Col span={8}><h3>Chat</h3></Col>
            <Col span={7} offset={9}>
              <Space size={35}>
                <p>Welcome <strong>{user.displayName}</strong></p>
                <Button onClick={() => dispatch(logoutUser())}>Log out</Button>
              </Space>
            </Col>
          </Row>
        </Header>
        <Content style={{ margin: '24px 16px 0' }}>
          <div style={{ padding: 24, minHeight: 360 }}>
            {children}
          </div>
        </Content>
        <Footer style={{ textAlign: 'center', backgroundColor: '#fff' }} />
      </Layout>
    </div>
  );
};

export default Lay;
