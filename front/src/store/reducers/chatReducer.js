import {
  GET_ALL_MESSAGES,
  GET_ALL_USERS,
  GET_MESSAGE_SUCCESS, GET_USER,
  SEND_MESSAGE_ERROR,
  SEND_MESSAGE_SUCCESS
} from "../actions/chatActions";

const initialState = {
  messages: [],
  users: [],
  error: null
};

const chatReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_ALL_MESSAGES:
      return {...state, messages: action.messages, error: null};
    case GET_MESSAGE_SUCCESS:
      return {...state, messages: [...state.messages, action.message]};
    case SEND_MESSAGE_SUCCESS:
      return {...state, messages: [...state.messages, action.message]};
    case SEND_MESSAGE_ERROR:
      return {...state, error: action.message};
    case GET_ALL_USERS:
      return {...state, users: action.users};
    case GET_USER:
      return {...state, users: [...state.users, action.user]};
    default:
      return state;
  }
};

export default chatReducer;
