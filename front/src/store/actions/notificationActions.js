import {notification} from "antd";

export const successNotification = title => {
  return () => {
    notification.success({
      message: 'Success',
      description: title,
    });
  };
};

export const errorNotification = title => {
  return () => {
    notification.error({
      message: 'Error',
      description: title,
    });
  };
};
