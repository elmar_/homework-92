export const SEND_MESSAGE_SUCCESS = 'SEND_MESSAGE_SUCCESS';
export const SEND_MESSAGE_ERROR = 'SEND_MESSAGE_ERROR';

export const GET_MESSAGE_SUCCESS = 'GET_MESSAGE_SUCCESS';
export const GET_MESSAGE_ERROR = 'GET_MESSAGE_ERROR';

export const GET_ALL_MESSAGES = 'GET_ALL_MESSAGES';

export const GET_ALL_USERS = 'GET_ALL_USERS';

export const GET_USER = 'GET_USER';

export const sendMessageError = error => ({type: SEND_MESSAGE_ERROR, error});

export const getMessageError = error => ({type: GET_MESSAGE_ERROR, error});



