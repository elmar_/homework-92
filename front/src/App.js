import React from 'react';
import {Switch, Route, Redirect} from "react-router-dom";
import {useSelector} from "react-redux";
import Chat from "./containers/Chat/Chat";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";

const ProtectedRoute = ({isAllowed, redirectTo, ...props}) => {
  return isAllowed ?
    <Route {...props} /> :
    <Redirect to={redirectTo}/>;
};

const App = () => {
  const user = useSelector(state => state.users.user);

  return (
    <div>
      <Switch>
        <ProtectedRoute
          path="/"
          exact
          component={Chat}
          isAllowed={user}
          redirectTo="/login"
        />
        <Route path="/register" component={Register}/>
        <Route path="/login" component={Login}/>
      </Switch>
    </div>
  );
};

export default App;
