import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {registerUser} from "../../store/actions/usersActions";
import {Alert, Button, Form, Input} from "antd";
import {Link} from "react-router-dom";

const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 8,
  },
};
const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 8,
  },
};

const Register = () => {
  const loading = useSelector(state => state.users.registerLoading);
  const error = useSelector(state => state.users.registerError);
  const dispatch = useDispatch();

  const onFinish = data => {
    dispatch(registerUser(data));
  };


  return (
    <div style={{marginTop: 200}}>
      <Form
        {...layout}
        onFinish={onFinish}
      >
        {<Form.Item {...tailLayout}>
          {error && <Alert
            message="Error Text"
            description={error.message || error.global || "Error, try again"}
            type="error"
          />}
        </Form.Item>}
        <Form.Item
          label="Email"
          name="email"
          type="email"
          rules={[
            {
              required: true,
              message: 'Please input your email!',
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Display name"
          name="displayName"
          rules={[
            {
              required: true,
              message: 'Please input your display name!',
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Password"
          name="password"
          rules={[
            {
              required: true,
              message: 'Please input your password!',
            },
          ]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item {...tailLayout}>
          <Button type="primary" htmlType="submit" loading={loading}>
            Register
          </Button>
        </Form.Item>

        <Form.Item {...tailLayout}>
          <Link to='/login'>Do you already have account?</Link>
        </Form.Item>

      </Form>
    </div>
  );
};

export default Register;
