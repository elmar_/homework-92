import React from 'react';
import {Alert, Button, Form, Input} from "antd";
import {useDispatch, useSelector} from "react-redux";
import {Link} from "react-router-dom";
import {loginUser} from "../../store/actions/usersActions";

const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 8,
  },
};
const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 8,
  },
};

const Login = () => {
  const loading = useSelector(state => state.users.loginLoading);
  const error = useSelector(state => state.users.loginError);
  const dispatch = useDispatch();

  const onFinish = data => {
    dispatch(loginUser(data));
  };


  return (
    <div style={{marginTop: 200}}>
      <Form
        {...layout}
        onFinish={onFinish}
      >
        {<Form.Item {...tailLayout}>
          {error && <Alert
            message="Error Text"
            description={error.message || error.global || "Error, try again"}
            type="error"
          />}
        </Form.Item>}

        <Form.Item
          label="Email"
          name="email"
          type="email"
          rules={[
            {
              required: true,
              message: 'Please input your email!',
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Password"
          name="password"
          rules={[
            {
              required: true,
              message: 'Please input your password!',
            },
          ]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item {...tailLayout}>
          <Button type="primary" htmlType="submit" loading={loading}>
            Login
          </Button>
        </Form.Item>

        <Form.Item {...tailLayout}>
          <Link to='/register'>Dont have account?</Link>
        </Form.Item>

      </Form>
    </div>
  );
};

export default Login;
