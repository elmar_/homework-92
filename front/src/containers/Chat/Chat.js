import React, {useEffect, useRef} from 'react';
import Lay from "../../components/Lay/Lay";
import {Alert, Button, Col, Form, Input, List, Row} from "antd";
import Title from "antd/es/typography/Title";
import {useDispatch, useSelector} from "react-redux";
import {sendMessageError} from "../../store/actions/chatActions";

const Chat = () => {
  const ws = useRef(null);
  const dispatch = useDispatch();
  const {messages, users, error} = useSelector(state => state.chat);
  const user = useSelector(state => state.users.user);

  useEffect(() => {
    ws.current = new WebSocket('ws://localhost:8000/chat?token=' + user.token);
    // ws.current.send(JSON.stringify({type: 'FIRST_CONNECT', user}));
    console.log('Используйте fixture');

    ws.current.onmessage = event => {
      const decode = JSON.parse(event.data);
      if (decode.type) {
        dispatch(decode);
      }
    };
  }, [dispatch, user.token]);

  const onFinish = message => {
    try {
      ws.current.send(JSON.stringify({type: 'SEND_MESSAGE', ...message, user}));
    } catch (e) {
      dispatch(sendMessageError(e));
    }
  };

  return (
    <Lay>
      {error && <Alert type="error" description={error} />}
      <Row gutter={[20, 0]}>
        <Col span={8}>
          <Title  level={4}>Online users</Title>
          <Col style={{border: '1px solid black'}}>
            <List
              size="small"
              dataSource={users}
              renderItem={user => <List.Item>{user}</List.Item>}
            />
          </Col>
        </Col>
        <Col span={16}>
          <Title level={4}>Chat</Title>
          <Col style={{border: '1px solid black'}}>
            <List
              size="small"
              dataSource={messages}
              renderItem={message => <List.Item><strong>{message.user}:</strong> {message.message} </List.Item>}
            />
          </Col>
          <Form style={{marginTop: 20}} onFinish={onFinish}>
            <Form.Item
              name="message"
              rules={[{ required: true, message: 'Please input message!' }]}
            >
              <Input
                placeholder="Enter message here"
                size="large"
                addonAfter={<Button type="text" htmlType="submit">Send</Button>}
              />
            </Form.Item>
          </Form>
        </Col>
      </Row>
    </Lay>
  );
};

export default Chat;
